import React from 'react';
import ReactDOM from 'react-dom';
import './styles/index.css';
import Main from "./components/route";
import Header from "./components/header";
import { BrowserRouter as Router } from 'react-router-dom';

export default class Layout extends React.Component {
	render(){
		var base = '/';
		return (
			<Router basename={base}>
				<div>
			    	<Header />
			    	<Main />
			    </div>
			</Router>
		)
	}
}

ReactDOM.render(<Layout />, document.getElementById('root'));
