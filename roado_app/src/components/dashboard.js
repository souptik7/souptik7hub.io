import React, { Component } from 'react';
import '../styles/index.css';
import '../styles/dashboard.css';

export default class Dashboard extends Component {
    constructor(){
      super();
      var origin = window.location.origin;
      if(localStorage.getItem('isLoggedIn') !== 'true')  window.location.href = origin;
    }
   render() {
        return (
          <div className="dashbpardDiv">
            <h2>Dashboard</h2>
            <div className="info_div">
              <div className="info_section">192</div>
              <p>No of Total Boxes</p>
            </div>
            <div className="info_div">
              <div className="info_section">35</div>
              <p>No of Empty Boxes</p>
            </div>
            <div className="info_div">
              <div className="info_section">79</div>
              <p>No of Full Boxes</p>
            </div>
            <div className="info_div">
              <div className="info_section">65</div>
              <p>No of Half Filled Boxes</p>
            </div>
          </div>
        )
    }
}
