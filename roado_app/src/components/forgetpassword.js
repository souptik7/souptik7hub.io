import React, { Component } from 'react';
import '../styles/index.css';
import '../styles/forgetpassword.css';
import { Link } from 'react-router-dom';

export default class Forget extends Component {
    constructor(){
      super();
      if(localStorage.getItem('isLoggedIn') === 'true')  window.location.href="dashboard";
      this.state = {
        mobile:'',
        otp:'',
        setotp:'123456',
        forgetSuccess:false,
        showOTPsection:false,
        otpErr:false,
        mobileErr:false
      }
    }
    handleNumberChange = (e) => {
      const reg1 = /^[0-9\b]+$/;
      if (e.target.value === '' || reg1.test(e.target.value)) {
         this.setState({mobile: e.target.value});
      }
    }
    handleOTPChange = (e) => {
      const reg2 = /^[0-9\b]+$/;
      if (e.target.value === '' || reg2.test(e.target.value)) {
         this.setState({otp: e.target.value});
      }
    }
    resetPassword = () => {
      if(this.state.mobile === undefined || this.state.mobile === '') return;
      const mobileReg = /^[6-9]{1}[0-9]{9}$/;
      if(!mobileReg.test(this.state.mobile)) return this.setState({mobileErr: true});
      this.setState({mobileErr: false});
      this.setState({showOTPsection: true});
    }
    verifyOTP = () => {
      if(this.state.otp === undefined || this.state.otp === '') return;
      if(this.state.otp.length !== 6 || this.state.otp !== '123456') return this.setState({otpErr: true});
      this.setState({otpErr: false});
      var tempLoginDetails = JSON.parse(localStorage.getItem('loginDetails'));
      for(var i=0;i<tempLoginDetails.length;i++){
        if(tempLoginDetails[i].mobile === this.state.mobile) tempLoginDetails[i].password = '123456';
      }
      localStorage.setItem('loginDetails',JSON.stringify(tempLoginDetails));
      this.setState({forgetSuccess: true});
    }
    render() {
        return (
            <div className="forgetDiv">
              <h2>Reset password</h2>
              {!this.state.forgetSuccess?<form>
                <p>
                  <span>Enter mobile number to reset the password.</span><br />
                  <input type="tel" value={this.state.mobile} onChange={this.handleNumberChange} maxLength="10" minLength="10"/>
                  {this.state.mobileErr?<span className="error">* Enter valid mobile number</span>:null}
                </p>
                {!this.state.showOTPsection?<button type="button" onClick={this.resetPassword}>Reset</button>:null}
                {this.state.showOTPsection?<p>
                  <span>Enter OTP to verify your mobile number</span><br />
                  <input type="tel" value={this.state.otp} onChange={this.handleOTPChange} maxLength="6" minLength="6"/>
                  {this.state.otpErr?<span className="error">* Invalid OTP. Try again.</span>:null}
                  <br /><button type="button" onClick={this.verifyOTP}>Verify</button>
                </p>:null}
              </form>:
              <p>
                <span>Password has been reset successfully. Please <Link to={''}>login</Link> using your mobile number <br /> and newly sent password.</span>
              </p>}
            </div>
        )
    }
}
