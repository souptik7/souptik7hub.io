import React, { Component } from 'react';
import '../styles/header.css';
import user from '../assets/user.png';

export default class Header extends Component {
    constructor(){
      super();
      this.state = {
        showMenu: false,
        showIcon: ''
      }
    }
    componentDidMount(){
      var flag = window.location.pathname === '/dashboard'?true:false;
      this.setState({showIcon: flag});
    }
    showAcountMenu = () => {
      this.setState({showMenu: !this.state.showMenu});
    }
    doLogout = () => {
      localStorage.setItem('isLoggedIn','false');
      var origin = window.location.origin;
      window.location.href = origin;
    }
    render() {
        return (
            <div className="header">
              <span>Roado Frontend Challenge</span>
              {this.state.showIcon?<img src={user} className="user_button" alt="account" onClick={this.showAcountMenu} />:null}
              {this.state.showMenu?<div className="menu_drop" onClick={this.doLogout}>
                <span>Log out</span>
              </div>:null}
            </div>
        );
    }
}
