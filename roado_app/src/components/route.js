import React, { Component } from 'react';
import Register from './register';
import Login from './login';
import Forget from './forgetpassword';
import Dashboard from './dashboard'
import { Route } from 'react-router-dom';

export default class Main extends Component {
    render() {
        return (
            <div>
              <Route exact path="/" render={()=>{return <Login />}}/>
              <Route exact path="/register" render={()=>{return <Register />}}/>
              <Route exact path="/forgetpassword" render={()=>{return <Forget />}}/>
              <Route exact path="/dashboard" render={()=>{return <Dashboard />}}/>
            </div>
        )
    }
}
