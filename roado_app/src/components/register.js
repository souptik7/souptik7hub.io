import React, { Component } from 'react';
import '../styles/index.css';
import '../styles/register.css';
import { Link } from 'react-router-dom';

export default class Register extends Component {
    constructor(){
      super();
      if(localStorage.getItem('isLoggedIn') === 'true')  window.location.href="dashboard";
      this.state = {
        name:'',
        mobile:'',
        email:'',
        password:'',
        repassword:'',
        mobileErr:false,
        emailErr:false,
        passwordErr:false,
        repasswordErr:false,
        mandErr:false,
        registerSuccess:false,
        duplicateErr: false
      };
    }
    handleNameChange = (e) => {
      const re = /^[a-zA-Z ]+$/;
      if (e.target.value === '' || re.test(e.target.value)) {
         this.setState({name: e.target.value});
      }
    }
    handleNumberChange = (e) => {
      const reg1 = /^[0-9\b]+$/;
      if (e.target.value === '' || reg1.test(e.target.value)) {
         this.setState({mobile: e.target.value});
      }
   }
   handleEmailChange = (e) => {
     this.setState({email: e.target.value});
   }
   handlePasswordChange =  (e) => {
     const reg2 = /^[a-zA-Z0-9@\b]+$/;
     if (e.target.value === '' || reg2.test(e.target.value)) {
        this.setState({password: e.target.value});
     }
   }
   handleRePasswordChange =  (e) => {
     this.setState({repassword: e.target.value});
   }
   doRegister = () => {
     if(this.state.name === undefined || this.state.name === '' || this.state.mobile === undefined || this.state.mobile === '' || this.state.password === undefined || this.state.password === '' || this.state.repassword === undefined || this.state.repassword === '') return this.setState({mandErr: true});
      this.setState({mandErr: false});
      const mobileReg = /^[6-9]{1}[0-9]{9}$/;
      if(!mobileReg.test(this.state.mobile)) return this.setState({mobileErr: true});
      this.setState({mobileErr: false});
      if(this.state.email || this.state.emailErr === true){
        const emailReg = /^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$/;
        if(!emailReg.test(this.state.email)) return this.setState({emailErr: true});
      }
      this.setState({emailErr: false});
      if(this.state.password.length < 6) return this.setState({passwordErr: true});
      this.setState({passwordErr: false});
      if(this.state.password !== this.state.repassword) return this.setState({repasswordErr: true});
      this.setState({repasswordErr: false});
      var tempLoginDetails = JSON.parse(localStorage.getItem('loginDetails'));
      var count = 0;
      for(var i=0;i<tempLoginDetails.length;i++){
        if(tempLoginDetails[i].mobile === this.state.mobile) count++;
      }
      if(count > 0) this.setState({duplicateErr: true});
      this.setState({duplicateErr: false});
      var data = {
        'name':this.state.name,
        'mobile':this.state.mobile,
        'email':this.state.email,
        'password':this.state.password
      }
      tempLoginDetails.push(data);
      localStorage.setItem('loginDetails',JSON.stringify(tempLoginDetails));
      localStorage.setItem('isLoggedIn','true');
      window.location.href="dashboard";
   }
   render() {
        return (
            <div className="registerDiv">
              {!this.state.registerSuccess?<form>
                <h2>Registration Form</h2>
                <p>
                  <span>Full Name <span className="error">*</span></span><br />
                  <input type="text" value={this.state.name} onChange={this.handleNameChange} maxLength="50" minLength="2" required="required"/>
                </p>
                <p>
                  <span>Mobile number <span className="error">*</span></span><br />
                  <input type="tel" value={this.state.mobile} onChange={this.handleNumberChange} maxLength="10"
                  minLength="10" required="required"/>
                  {this.state.mobileErr?<span className="error">* Enter valid mobile number</span>:null}
                </p>
                <p>
                  <span>Email Id</span><br />
                  <input type="email" value={this.state.email} onChange={this.handleEmailChange} maxLength="50" />
                  {this.state.emailErr?<span className="error">* Enter valid email id</span>:null}
                </p>
                  <p><span>Password <span className="error">*</span></span><br />
                  <input type="password" value={this.state.password} onChange={this.handlePasswordChange} maxLength="32" minLength="6" required="required"/>
                  {this.state.passwordErr?<span className="error">* Enter valid password</span>:null}
                </p>
                <p>
                  <span>Re-enter password <span className="error">*</span></span><br />
                  <input type="password" value={this.state.repassword} onChange={this.handleRePasswordChange} maxLength="32" minLength="6" required="required"/>
                  {this.state.repasswordErr?<span className="error">* Passwords are not matching</span>:null}
                </p>
                <p className="mandatoryClass">{this.state.mandErr?<span className="error">* Fill in all mandatory fields</span>:null}{this.state.duplicateErr?<span className="error">* Mobile number already exists.</span>:null}</p>
                <button type="button" onClick={this.doRegister}>Submit</button>
                <p><Link to={''}><span>Already registered? Sign in Here!</span></Link></p>
              </form>:
              <p className="registerSuccessClass">
                <h2>Registration Successful</h2>
                <span>You have been successfully registered. Please <Link to={''}>login</Link> using your mobile number <br /> and given password.</span>
              </p>}
            </div>
        )
    }
}
