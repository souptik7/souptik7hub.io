import React, { Component } from 'react';
import '../styles/index.css';
import '../styles/login.css';
import { Link } from 'react-router-dom';

export default class Login extends Component {
    constructor(){
      super();
      if(localStorage.getItem('isLoggedIn') === 'true')  window.location.href="dashboard";
      this.state = {
        loginDetails: [],
        mobile:'',
        password:'',
        mobileErr:false,
        passwordErr:false
      }
      this.state.loginDetails = localStorage.getItem('loginDetails') === undefined?[{name:'Roado Frontend Challenge',mobile:'9999999999',email:'',password:'123456'}]:JSON.parse(localStorage.getItem('loginDetails'));
      localStorage.setItem('loginDetails', JSON.stringify(this.state.loginDetails));
    }
    handleNumberChange = (e) => {
      const reg1 = /^[0-9\b]+$/;
      if (e.target.value === '' || reg1.test(e.target.value)) {
         this.setState({mobile: e.target.value});
      }
   }
   handlePasswordChange = (e) => {
     const reg2 = /^[a-zA-Z0-9@\b]+$/;
     if (e.target.value === '' || reg2.test(e.target.value)) {
        this.setState({password: e.target.value});
     }
   }
   dologin = () => {
     if(this.state.mobile ===  undefined || this.state.mobile ===  '' || this.state.password ===  undefined || this.state.password ===  '') return;
     const mobileReg = /^[6-9]{1}[0-9]{9}$/;
     if(!mobileReg.test(this.state.mobile)) return this.setState({mobileErr: true});
     this.setState({mobileErr: false});
     var count = 0;
     var loginDetails = this.state.loginDetails;
     for(var i=0;i<loginDetails.length;i++){
       if(this.state.mobile === loginDetails[i].mobile && this.state.password === loginDetails[i].password) count++;
     }
     if(count > 0){
       this.setState({passwordErr: false});
       window.location.href="dashboard";
       localStorage.setItem('isLoggedIn','true');
     } else {
       this.setState({passwordErr: true});
     }
   }
   render() {
        return (
            <div className="loginDiv">
              <h2>Login</h2>
              <form>
                <p>
                  <span>Mobile Number</span><br />
                  <input type="tel" value={this.state.mobile} onChange={this.handleNumberChange} maxLength="10" minLength="10" required="required"/>
                  {this.state.mobileErr?<span className="error">* Enter valid mobile number</span>:null}
                </p>
                <p>
                  <span>Password</span><br />
                  <input type="password" value={this.state.password} onChange={this.handlePasswordChange} maxLength="32" minLength="6" required="required"/>
                  {this.state.passwordErr?<span className="error">* Given password is incorrect</span>:null}
                </p>
                <p className="forgetLink">
                  <Link to={'forgetpassword'}><span>Forgot password?</span></Link>
                </p>
                <button type="button" onClick={this.dologin}>Login</button>
              </form>
              <p><Link to={'register'}><span>New to Roado? Register Here!</span></Link></p>
            </div>
        )
    }
}
